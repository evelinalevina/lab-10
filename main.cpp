﻿#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include "class.hpp"
#include <vector>

using namespace std::chrono_literals;


int main()
{
    srand(time(0));
    setlocale(LC_ALL, "Rus");

    // Определение числа N
    int N = rand() % 100 + 1;

    // Создание окна
    sf::RenderWindow window(sf::VideoMode(800, 600), L"Лабораторная работа 10");

    // Создание и расположение N треугольников 
    std::vector<el::Triangle*> triangles;
    for (int i = 0; i <= 600; i += 600 / N)
        triangles.push_back(new el::Triangle(0, i, 15, rand() % 10 + 1)); 

    // Обработка события
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        // Движение треугольников
        for (const auto& triangle : triangles)
        {
            triangle->Move();
            if (triangle->GetX() > 800)
            {
                triangle->SetVelocity(rand() % 10 + 1);
                triangle->SetX(0);
            }
        }

        // Очистка окна
        window.clear();
 
        // Перемещение фигур в буфер
        for (const auto& triangle : triangles)
            window.draw(*triangle->Get());
        window.display();

        std::this_thread::sleep_for(40ms);
    }

    // Очистка
    for (const auto& triangle : triangles)
        delete triangle;
    triangles.clear();

    return 0;
}