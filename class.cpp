#include "class.hpp"

namespace el
{
	Triangle::Triangle(int x, int y, float r, float velocity)
	{
		m_x = x;
		m_y = y;
		m_r = r;
		m_velocity = velocity;
		m_shape = new sf::CircleShape(m_r, 3);
		m_shape->setOrigin(m_r, m_r);
		m_shape->setFillColor(sf::Color::Red);
		m_shape->setPosition(m_x, m_y);
	}
	Triangle::~Triangle()
	{
		delete m_shape;
	}

	sf::CircleShape* Triangle::Get() { return m_shape; }

	void Triangle::Move()
	{
		m_x += m_velocity;
		m_shape->setPosition(m_x, m_y);
		if (m_x > 800)
			m_x = 800;
		m_shape->setPosition(m_x, m_y);
	}

	void Triangle::SetX(int x)
	{
		m_x = x;
		m_shape->setPosition(m_x, m_y);
	}

	int Triangle::GetX() { return m_x; }

	void Triangle::SetVelocity(int velocity)
	{
		m_velocity = velocity;
	}
}
