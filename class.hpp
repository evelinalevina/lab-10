#pragma once
#include <SFML/Graphics.hpp>


namespace el
{
	class Triangle
	{
	public:
		Triangle(int x, int y, float r, float velocity);

		~Triangle();

		sf::CircleShape* Get();

		void Move();

		void SetX(int x);

		int GetX();

		void SetVelocity(int velocity);

	private:
		int m_x, m_y;
		float m_r;
		float m_velocity;
		sf::CircleShape* m_shape;
	};

}

